# docker-jq

super small and simple jq in docker.

>jq is like sed for JSON data - you can use it to slice and filter and map and transform structured data with the same ease that sed, awk, grep and friends let you play with text.

really tired of how many innocent docker images are left at a certain point in time with no maintenance, picking up plenty of awful vulnerabilities over time.

this one re-builds itself. it also takes the latest jq available. i don't think anyone will really care about jq 1.5 vs 1.6 for example.

## usage

https://stedolan.github.io/jq/manual/

```
alias jq="docker run --rm -i registry.gitlab.com/adrift/docker-jq"

jq
```

maybe update your local one once in a while

```
docker pull registry.gitlab.com/adrift/docker-jq
```