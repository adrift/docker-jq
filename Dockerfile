FROM alpine

RUN apk --update add jq && rm -rf /var/cache/apk/*

ENTRYPOINT ["jq"]
CMD ["-h"]